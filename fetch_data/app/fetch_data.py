import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    housing = pickle.load(open(f"{PICKLE_PATH}/housing.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = fetch_data
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["housing"]

# Python ≥3.5 is required
assert sys.version_info >= (3, 5)

# Scikit-Learn ≥0.20 is required
assert sklearn.__version__ >= "0.20"


# To plot pretty figures
#%matplotlib inline

mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

# Where to save the figures
PROJECT_ROOT_DIR = "."
CHAPTER_ID = "end_to_end_project"
IMAGES_PATH = os.path.join(PROJECT_ROOT_DIR, "images", CHAPTER_ID)
os.makedirs(IMAGES_PATH, exist_ok=True)

def save_fig(fig_id, tight_layout=True, fig_extension="png", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)

# Ignore useless warnings (see SciPy issue #5998)
warnings.filterwarnings(action="ignore", message="^internal gelsd")

DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml2/master/"
HOUSING_PATH = os.path.join("datasets", "housing")
HOUSING_URL = DOWNLOAD_ROOT + "datasets/housing/housing.tgz"

def fetch_housing_data(housing_url=HOUSING_URL, housing_path=HOUSING_PATH):
    if not os.path.isdir(housing_path):
        os.makedirs(housing_path)
    tgz_path = os.path.join(housing_path, "housing.tgz")
    urllib.request.urlretrieve(housing_url, tgz_path)
    housing_tgz = tarfile.open(tgz_path)
    housing_tgz.extractall(path=housing_path)
    housing_tgz.close()
fetch_housing_data()

def load_housing_data(housing_path=HOUSING_PATH):
    csv_path = os.path.join(housing_path, "housing.csv")
    return pd.read_csv(csv_path)
housing = load_housing_data()
housing.head()
housing.info()
housing["ocean_proximity"].value_counts()
housing.describe()
#%matplotlib inline

housing.hist(bins=50, figsize=(20,15))
save_fig("attribute_histogram_plots")
xpresso_save_plot("image_1", output_path=os.environ['XPRESSO_MOUNT_PATH'],output_folder="xjp_images/fetch_data")

try:
    pickle.dump(housing, open(f"{PICKLE_PATH}/housing.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

